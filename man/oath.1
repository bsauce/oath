.TH OATH 1 "2018 August 21" BinarySauce "oath-key-store"

.SH NAME
oath - securely stores, retrieves, and synchronizes TOTP (timed one time 
password) 2FA keys  

.SH SYNOPSIS
.B oath
[ 
.I COMMAND
] [ 
.I OPTIONS
]... [ 
.I ARGS
]...

.SH DESCRIPTION

.B oath 
is a TOTP 2FA key store that keeps 2FA keys 
.BR gpg2 (1)
encrypted inside a simple directory tree residing at 
.IR ~/.oath-key-store .
.B oath 
is a companion to the standard unix password manager
.BR pass 
(https://www.passwordstore.org/).  It requires 
.BR oathtool
(https://www.nongnu.org/oath-toolkit/) to calculate passwords. 
The
.B oath
utility provides a series of commands for adding, removing, editing, organising
and synchronizing stored keys.

If no COMMAND is specified, COMMAND defaults to either
.B show
or
.B ls ,
depending on the type of specifier in ARGS. Otherwise COMMAND must be one of the
valid commands listed below.

Several of the commands below rely on or provide additional functionality if the 
.B oath 
key-store directory is also a git repository. When 
.B oath's 
key-store directory is a git repository, all 
.B oath 
key-store commands will cause a corresponding git commit. Sub-directories may 
be separate nested git repositories, and oath will use the inner-most directory 
relative to the current password. See the \fIEXTENDED GIT EXAMPLE\fP section for 
a detailed description using \fBinit\fP and
.BR git (1).

The \fBinit\fP command must be run before other commands in order to initialize
the 
.B oath 
key-store with the correct gpg key id. Passwords are encrypted using
the gpg key set with \fBinit\fP.

There is a corresponding bash completion script for use with tab completing 
names in
.BR bash (1).

.SH COMMANDS

.TP
\fBinit\fP [ \fI--path=sub-folder\fP, \fI-p sub-folder\fP ] \fIgpg-id...\fP
Initialize new key storage and use
.I gpg-id
for encryption. Multiple gpg-ids may be specified, in order to encrypt each
password with multiple ids. This command must be run first before a password
store can be used. If the specified \fIgpg-id\fP is different from the key used 
in any existing files, these files will be reencrypted to use the new id.  Note 
that use of
.BR gpg-agent (1)
is recommended so that the batch decryption does not require as much user
intervention. If \fI--path\fP or \fI-p\fP is specified, along with an argument,
a specific gpg-id or set of gpg-ids is assigned for that specific sub folder of
the 
.B oath 
key-store. If only one \fIgpg-id\fP is given, and it is an empty 
string, then the current \fI.gpg-id\fP file for the specified \fIsub-folder\fP 
(or root if unspecified) is removed. 
.B DO NOT USE THE SAME GPG KEYS USED TO STORE PASSWORDS IN PASS(1)
.TP
\fBls\fP \fIsubfolder\fP
List names of keys inside the tree at
.I subfolder
by using the
.BR tree (1)
program. This command is alternatively named \fBlist\fP.
.TP
\fBfind\fP \fIkey-names\fP...
List names of passwords inside the tree that match \fIkey-names\fP by using the
.BR tree (1)
program. This command is alternatively named \fBsearch\fP.
.TP
\fBshow\fP [ \fI--clip\fP[=\fIline-number\fP], \fI-c\fP[\fIline-number\fP] ] [ \fI--qrcode\fP[=\fIline-number\fP], \fI-q\fP[\fIline-number\fP] ] \fIkey-name\fP
Decrypt and print a password named \fIkey-name\fP. If \fI--clip\fP or \fI-c\fP
is specified, do not print the password but instead copy the first (or otherwise specified)
line to the clipboard using
.BR xclip (1)
and then restore the clipboard after 45 (or \fIOATH_KEY_STORE_CLIP_TIME\fP) seconds. If \fI--qrcode\fP
or \fI-q\fP is specified, do not print the password but instead display a QR code using
.BR qrencode (1)
either to the terminal or graphically if supported.
.TP
\fBinsert\fP [ \fI--force\fP, \fI-f\fP ] \fIkey-name\fP
Insert a new password into the oath key store called \fIkey-name\fP. This will
read the new password from standard in. Prompt before overwriting an existing password, unless 
\fI--force\fP or \fI-f\fP is specified. This command is alternatively 
named \fBadd\fP.
.TP
\fBedit\fP \fIkey-name\fP
Insert a new password or edit an existing password using the default text editor specified
by the environment variable \fIEDITOR\fP or using
.BR vi (1)
as a fallback. This mode makes use of temporary files for editing, but care is taken to
ensure that temporary files are created in \fI/dev/shm\fP in order to avoid writing to
difficult-to-erase disk sectors. If \fI/dev/shm\fP is not accessible, fallback to
the ordinary \fITMPDIR\fP location, and print a warning.
.TP
\fBrm\fP [ \fI--recursive\fP, \fI-r\fP ] [ \fI--force\fP, \fI-f\fP ] \fIkey-name\fP
Remove the password named \fIkey-name\fP from the oath key store. This command is
alternatively named \fBremove\fP or \fBdelete\fP. If \fI--recursive\fP or \fI-r\fP
is specified, delete oath-name recursively if it is a directory. If \fI--force\fP
or \fI-f\fP is specified, do not interactively prompt before removal.
.TP
\fBmv\fP [ \fI--force\fP, \fI-f\fP ] \fIold-path\fP \fInew-path\fP
Renames the password or directory named \fIold-path\fP to \fInew-path\fP. This
command is alternatively named \fBrename\fP. If \fI--force\fP is specified,
silently overwrite \fInew-path\fP if it exists. If \fInew-path\fP ends in a
trailing \fI/\fP, it is always treated as a directory. Passwords are selectively
reencrypted to the corresponding keys of their new destination.
.TP
\fBcp\fP [ \fI--force\fP, \fI-f\fP ] \fIold-path\fP \fInew-path\fP
Copies the password or directory named \fIold-path\fP to \fInew-path\fP. This
command is alternatively named \fBcopy\fP. If \fI--force\fP is specified,
silently overwrite \fInew-path\fP if it exists. If \fInew-path\fP ends in a
trailing \fI/\fP, it is always treated as a directory. Passwords are selectively
reencrypted to the corresponding keys of their new destination.
.TP
\fBgit\fP \fIgit-command-args\fP...
If the oath key store is a git repository, oath \fIgit-command-args\fP as arguments to
.BR git (1)
using the oath key store as the git repository. If \fIgit-command-args\fP is \fBinit\fP,
in addition to initializing the git repository, add the current contents of the password
store to the repository in an initial commit. If the git config key \fIpass.signcommits\fP
is set to \fItrue\fP, then all commits will be signed using \fIuser.signingkey\fP or the
default git signing key. This config key may be turned on using:
.B `oath git config --bool --add oath.signcommits true`
.TP
\fBhelp\fP
Show usage message.
.TP
\fBversion\fP
Show version information.

.SH SIMPLE EXAMPLES

.TP
Initialize oath key store
.B bsauce@mutant ~ $ oath init reply@binarysauce.com.au 
.br
mkdir: created directory \[u2018]/home/bsauce/.oath-key-store\[u2019] 
.br
Key store initialized for reply@binarysauce.com.au.
.TP
List existing passwords in store
.B bsauce@mutant ~ $ oath 
.br
Oath Key Store
.br
\[u251C]\[u2500]\[u2500] gitlab 
.br
\[u2502]   \[u251C]\[u2500]\[u2500] bsauce 
.br
\[u2502]   \[u2514]\[u2500]\[u2500] binsrc 
.br
\[u251C]\[u2500]\[u2500] github 
.br
\[u2502]   \[u251C]\[u2500]\[u2500] bsauce 
.br
\[u2502]   \[u2514]\[u2500]\[u2500] binsrc 
.br
\[u2514]\[u2500]\[u2500] gitea 
.br
    \[u251C]\[u2500]\[u2500] bsauce 
.br
    \[u251C]\[u2500]\[u2500] binsrc 
.br
    \[u2514]\[u2500]\[u2500] apples  
.br

.br
Alternatively, "\fBoath ls\fP".
.TP
Find existing passwords in store that match 
.I sauce
.B bsauce@mutant ~ $ oath find sauce
.br
Search Terms: sauce
.br
\[u251C]\[u2500]\[u2500] gitlab  
.br
\[u2502]   \[u2514]\[u2500]\[u2500] bsauce 
.br
\[u251C]\[u2500]\[u2500] github  
.br
\[u2502]   \[u2514]\[u2500]\[u2500] bsauce 
.br
\[u2514]\[u2500]\[u2500] gitea 
.br
    \[u2514]\[u2500]\[u2500] bsauce  
.br

.br
Alternatively, "\fBoath search sauce\fP".
.TP
Echo 2FA TOTP
.B bsauce@mutant ~ $ oath gitlab/bsauce 
.br
832664 
.TP
Copy 2FA TOTP to clipboard
.B bsauce@mutant ~ $ oath gitlab/bsauce -c
.br
Copied gitlab/bsauce to clipboard. Will clear in 45 seconds.
.TP
Add key to store
.B bsauce@mutant ~ $ oath insert gogs/binsrc 
.br
Enter key for gogs/binsrc: X9EH SJHQ O2JE CLJ5 L22T IOT4
.TP
.TP
Remove key from store
.B bsauce@mutant ~ $ oath remove gogs/binsrc 
.br
rm: remove regular file \[u2018]/home/bsauce/.oath-key-store/gogs/binsrc.gpg\[u2019]? y 
.br
removed \[u2018]/home/bsauce/.oath-key-store/gogs/binsrc.gpg\[u2019]

.SH FILES

.TP
.B ~/.oath-key-store
The default password storage directory.
.TP
.B ~/.oath-key-store/.gpg-id
Contains the default gpg key identification used for encryption and decryption.
Multiple gpg keys may be specified in this file, one per line. If this file
exists in any sub directories, keys inside those sub directories are
encrypted using those keys. This should be set using the \fBinit\fP command..

.SH ENVIRONMENT VARIABLES

.TP
.I OATH_KEY_STORE_DIR
Overrides the default key storage directory.
.TP
.I OATH_KEY_STORE_KEY
Overrides the default gpg key identification set by \fBinit\fP. Keys must not
contain spaces and thus use of the hexadecimal key signature is recommended.
Multiple keys may be specified separated by spaces. 
.TP
.I OATH_KEY_STORE_GPG_OPTS
Additional options to be passed to all invocations of GPG.
.TP
.I OATH_KEY_STORE_X_SELECTION
Overrides the selection passed to \fBxclip\fP, by default \fIclipboard\fP. See
.BR xclip (1)
for more info.
.TP
.I OATH_KEY_STORE_CLIP_TIME
Specifies the number of seconds to wait before restoring the clipboard, by default
\fI45\fP seconds.
.TP
.I OATH_KEY_STORE_UMASK
Sets the umask of all files modified by oath, by default \fI077\fP.
.TP
.I OATH_KEY_STORE_SIGNING_KEY
If this environment variable is set, then all \fB.gpg-id\fP files and non-system extension files
must be signed using a detached signature using the GPG key specified by the full 40 character
upper-case fingerprint in this variable. If multiple fingerprints are specified, each
separated by a whitespace character, then signatures must match at least one.
The \fBinit\fP command will keep signatures of \fB.gpg-id\fP files up to date.
.TP
.I EDITOR
The location of the text editor used by \fBedit\fP.
.SH SEE ALSO
.BR gpg2 (1),
.BR oathtool (1),
.BR tr (1),
.BR git (1),
.BR xclip (1),
.BR qrencode (1),
.BR pass (1).

.SH AUTHOR
.B oath
was written by
.MT reply@binarysauce.com.au
Binary Sauce
.ME .
For updates and more information, a project page is available on the
.UR https://\:binarysauce.com.au/oath.html
World Wide Web
.UE .

.SH COPYING
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

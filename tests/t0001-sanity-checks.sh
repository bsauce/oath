#!/usr/bin/env bash

test_description='Sanity checks'
cd "$(dirname "$0")"
. ./setup.sh

test_expect_success 'Make sure we can run pass' '
	"$OATH" --help | grep "oath: 2FA timed one time password manager"
'

test_expect_success 'Make sure we can initialize our test store' '
	"$OATH" init $KEY1 &&
	[[ -e "$OATH_KEY_STORE_DIR/.gpg-id" ]] &&
	[[ $(cat "$OATH_KEY_STORE_DIR/.gpg-id") == "$KEY1" ]]
'

test_done

# This file should be sourced by all test-scripts
#
# This scripts sets the following:
#   $PASS	Full path to password-store script to test
#   $GPG	Name of gpg executable
#   $KEY{1..5}	GPG key ids of testing keys
#   $TEST_HOME	This folder


# Unset config vars
unset OATH_KEY_STORE_DIR
unset OATH_KEY_STORE_KEY
unset OATH_KEY_STORE_GIT
unset OATH_KEY_STORE_GPG_OPTS
unset OATH_KEY_STORE_X_SELECTION
unset OATH_KEY_STORE_CLIP_TIME
unset OATH_KEY_STORE_UMASK
unset OATH_KEY_STORE_GENERATED_LENGTH
unset OATH_KEY_STORE_CHARACTER_SET
unset OATH_KEY_STORE_CHARACTER_SET_NO_SYMBOLS
unset OATH_KEY_STORE_ENABLE_EXTENSIONS
unset OATH_KEY_STORE_EXTENSIONS_DIR
unset OATH_KEY_STORE_SIGNING_KEY
unset EDITOR

# We must be called from tests/
TEST_HOME="$(pwd)"

. ./sharness.sh

export OATH_KEY_STORE_DIR="$SHARNESS_TRASH_DIRECTORY/test-store/"
rm -rf "$OATH_KEY_STORE_DIR"
mkdir -p "$OATH_KEY_STORE_DIR"
if [[ ! -d $OATH_KEY_STORE_DIR ]]; then
	echo "Could not create $OATH_KEY_STORE_DIR"
	exit 1
fi

export GIT_DIR="$OATH_KEY_STORE_DIR/.git"
export GIT_WORK_TREE="$OATH_KEY_STORE_DIR"
git config --global user.email "Pass-Automated-Testing-Suite@zx2c4.com"
git config --global user.name "Pass Automated Testing Suite"


PASS="$TEST_HOME/../src/oath.sh"
OATH="$TEST_HOME/../src/oath.sh"
if [[ ! -e $OATH ]]; then
	echo "Could not find oath.sh"
	exit 1
fi

# Note: the assumption is the test key is unencrypted.
export GNUPGHOME="$TEST_HOME/gnupg/"
chmod 700 "$GNUPGHOME"
GPG="gpg"
which gpg2 &>/dev/null && GPG="gpg2"

# We don't want any currently running agent to conflict.
unset GPG_AGENT_INFO

KEY1="CF90C77B"  # pass test key 1
KEY2="D774A374"  # pass test key 2
KEY3="EB7D54A8"  # pass test key 3
KEY4="E4691410"  # pass test key 4
KEY5="39E5020C"  # pass test key 5
